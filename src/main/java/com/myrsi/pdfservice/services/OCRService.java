package com.myrsi.pdfservice.services;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import java.io.File;

public class OCRService {

    public String extractAmountFromImage(File imageFile) {
        ITesseract tesseract = new Tesseract();

        try {
            String result = tesseract.doOCR(imageFile);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
