package com.myrsi.pdfservice.controllers;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/pdf")
public class PdfController {

    @PostMapping("/extractText")
    public ResponseEntity<PdfTextResponse> extractText(@RequestBody MultipartFile file) {
        try {
            // Convert MultipartFile to InputStream
            InputStream inputStream = file.getInputStream();

            // Load PDF document
            PDDocument document = PDDocument.load(inputStream);

            // Create PDFTextStripper
            PDFTextStripper stripper = new PDFTextStripper();

            // Extract text from the document
            String text = stripper.getText(document);

            // Close the document
            document.close();

            // Create and return a JSON response
            PdfTextResponse response = new PdfTextResponse(text);
            return ResponseEntity.ok(response);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(new PdfTextResponse("Error extracting text from PDF"));
        }
    }

    // Helper method to extract dates using a regular expression
    private Date extractDateFromText(String text) throws ParseException {
        Pattern datePattern = Pattern.compile("\\b\\d{1,2}/\\d{1,2}/\\d{4}\\b");
        Matcher matcher = datePattern.matcher(text);

        if (matcher.find()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            return dateFormat.parse(matcher.group());
        }

        return null;
    }

    // Helper method to extract amounts using a regular expression
    private Map<String, Double> extractAmountsFromText(String text) {
        Map<String, Double> amountMap = new HashMap<>();

        // Define regex patterns for individual amounts
        Pattern htPattern = Pattern.compile("Montant\\s*HT[^\\d]*(\\d+(?:[.,]\\d{1,2})?)", Pattern.CASE_INSENSITIVE);
        Pattern tauxPattern = Pattern.compile("Taux[^\\d]*(\\d+(?:[.,]\\d{1,2})?)", Pattern.CASE_INSENSITIVE);
        Pattern tvaPattern = Pattern.compile("Montant\\s*TVA[^\\d]*(\\d+(?:[.,]\\d{1,2})?)", Pattern.CASE_INSENSITIVE);
        Pattern ttcPattern = Pattern.compile("TTC[^\\d]*(\\d+(?:[.,]\\d{1,2})?)", Pattern.CASE_INSENSITIVE);

        // Match individual amounts
        Matcher htMatcher = htPattern.matcher(text);
        Matcher tauxMatcher = tauxPattern.matcher(text);
        Matcher tvaMatcher = tvaPattern.matcher(text);
        Matcher ttcMatcher = ttcPattern.matcher(text);

        if (htMatcher.find() && tauxMatcher.find() && tvaMatcher.find() && ttcMatcher.find()) {
            // Populate the map with specific keys and amounts
            amountMap.put("HT", Double.parseDouble(htMatcher.group(1).replace(",", ".")));
            amountMap.put("Taux", Double.parseDouble(tauxMatcher.group(1).replace(",", ".")));
            amountMap.put("TVA", Double.parseDouble(tvaMatcher.group(1).replace(",", ".")));
            amountMap.put("TTC", Double.parseDouble(ttcMatcher.group(1).replace(",", ".")));

            // Log matched amounts for debugging
            System.out.println("HT: " + amountMap.get("HT"));
            System.out.println("Taux: " + amountMap.get("Taux"));
            System.out.println("TVA: " + amountMap.get("TVA"));
            System.out.println("TTC: " + amountMap.get("TTC"));
        }

        return amountMap;
    }

    @PostMapping("/extractAmountsAndDates")
    public ResponseEntity<Map<String, Object>> extractAmountsAndDates(@RequestBody MultipartFile file) {
        try {
            // Convert MultipartFile to InputStream
            InputStream inputStream = file.getInputStream();

            // Load PDF document
            PDDocument document = PDDocument.load(inputStream);

            // Create PDFTextStripper
            PDFTextStripper stripper = new PDFTextStripper();

            // Extract text from the document
            String text = stripper.getText(document);

            // Close the document
            document.close();

            // Check if the extracted text is not null
            if (text != null) {
                // Extract amounts using a regular expression
                Map<String, Double> amountsMap = extractAmountsFromText(text);

                // Extract date using a regular expression
                Date date = extractDateFromText(text);

                // Create and return a JSON response with both amounts and date
                Map<String, Object> responseMap = new HashMap<>();
                responseMap.put("date", date != null ? formatDate(date) : null);
                responseMap.put("TTC", amountsMap.get("TTC"));
                // responseMap.put("Taux", amountsMap.get("Taux"));
                // responseMap.put("HT", amountsMap.get("HT"));
                // responseMap.put("TVA", amountsMap.get("TVA"));

                return ResponseEntity.ok(responseMap);
            } else {
                return ResponseEntity.status(500).body(Collections.emptyMap()); // or handle the error as needed
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(Collections.emptyMap()); // or handle the error as needed
        }
    }

    // Helper method to format dates
    private String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(date);
    }

    // DTO class for JSON response
    private static class PdfTextResponse {
        private final String text;

        public PdfTextResponse(String text) {
            this.text = text;
        }

    }
}
