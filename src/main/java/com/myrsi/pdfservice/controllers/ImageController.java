package com.myrsi.pdfservice.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.myrsi.pdfservice.services.OCRService;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/api/images")
public class ImageController {

    @PostMapping("/extract-amount")
    public String extractAmount(@RequestBody MultipartFile file) {
        try {
            // Convert MultipartFile to File
            File imageFile = convertMultipartFileToFile(file);

            // Perform OCR
            OCRService ocrService = new OCRService();
            String amount = ocrService.extractAmountFromImage(imageFile);

            return amount;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error processing image";
        }
    }

    private File convertMultipartFileToFile(MultipartFile file) throws IOException {
        File convertedFile = new File(file.getOriginalFilename());
        file.transferTo(convertedFile);
        return convertedFile;
    }

}